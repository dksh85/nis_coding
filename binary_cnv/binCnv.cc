#include "binCnv.h"

using namespace std;

void Alg_1 :: computeBin(int number){		
	int div = number;
	int mod = 0;
	int stack[100];
	int stackIdx = 0;


	while(div >= 2){
		mod = div % 2;
		div = div / 2;

		stack[stackIdx++] = mod;
	}
	stack[stackIdx] = div;

	for(int i = stackIdx; i >= 0; i--){
		cout << stack[i]; 
	}
	cout << endl;
}


int main(){
	BinCnv *bincnv;
	Alg_1 alg_1;

	bincnv = &alg_1;
	bincnv->computeBin(10);
}
