#ifndef _BINCNV_H_
#define _BINCNV_H_
#include <iostream>

class BinCnv {
	public:
		virtual void computeBin(int number) = 0; 	
};

class Alg_1 : public BinCnv{
	public:
		void computeBin(int number);
};

#endif
